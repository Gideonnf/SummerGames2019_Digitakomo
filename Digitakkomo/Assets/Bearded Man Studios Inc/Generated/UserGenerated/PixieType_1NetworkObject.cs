using BeardedManStudios.Forge.Networking.Frame;
using BeardedManStudios.Forge.Networking.Unity;
using System;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedInterpol("{\"inter\":[0.15]")]
	public partial class PixieType_1NetworkObject : NetworkObject
	{
		public const int IDENTITY = 9;

		private byte[] _dirtyFields = new byte[1];

		#pragma warning disable 0067
		public event FieldChangedEvent fieldAltered;
		#pragma warning restore 0067
		[ForgeGeneratedField]
		private Vector3 _serverPosition;
		public event FieldEvent<Vector3> serverPositionChanged;
		public InterpolateVector3 serverPositionInterpolation = new InterpolateVector3() { LerpT = 0.15f, Enabled = true };
		public Vector3 serverPosition
		{
			get { return _serverPosition; }
			set
			{
				// Don't do anything if the value is the same
				if (_serverPosition == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x1;
				_serverPosition = value;
				hasDirtyFields = true;
			}
		}

		public void SetserverPositionDirty()
		{
			_dirtyFields[0] |= 0x1;
			hasDirtyFields = true;
		}

		private void RunChange_serverPosition(ulong timestep)
		{
			if (serverPositionChanged != null) serverPositionChanged(_serverPosition, timestep);
			if (fieldAltered != null) fieldAltered("serverPosition", _serverPosition, timestep);
		}

		protected override void OwnershipChanged()
		{
			base.OwnershipChanged();
			SnapInterpolations();
		}
		
		public void SnapInterpolations()
		{
			serverPositionInterpolation.current = serverPositionInterpolation.target;
		}

		public override int UniqueIdentity { get { return IDENTITY; } }

		protected override BMSByte WritePayload(BMSByte data)
		{
			UnityObjectMapper.Instance.MapBytes(data, _serverPosition);

			return data;
		}

		protected override void ReadPayload(BMSByte payload, ulong timestep)
		{
			_serverPosition = UnityObjectMapper.Instance.Map<Vector3>(payload);
			serverPositionInterpolation.current = _serverPosition;
			serverPositionInterpolation.target = _serverPosition;
			RunChange_serverPosition(timestep);
		}

		protected override BMSByte SerializeDirtyFields()
		{
			dirtyFieldsData.Clear();
			dirtyFieldsData.Append(_dirtyFields);

			if ((0x1 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _serverPosition);

			// Reset all the dirty fields
			for (int i = 0; i < _dirtyFields.Length; i++)
				_dirtyFields[i] = 0;

			return dirtyFieldsData;
		}

		protected override void ReadDirtyFields(BMSByte data, ulong timestep)
		{
			if (readDirtyFlags == null)
				Initialize();

			Buffer.BlockCopy(data.byteArr, data.StartIndex(), readDirtyFlags, 0, readDirtyFlags.Length);
			data.MoveStartIndex(readDirtyFlags.Length);

			if ((0x1 & readDirtyFlags[0]) != 0)
			{
				if (serverPositionInterpolation.Enabled)
				{
					serverPositionInterpolation.target = UnityObjectMapper.Instance.Map<Vector3>(data);
					serverPositionInterpolation.Timestep = timestep;
				}
				else
				{
					_serverPosition = UnityObjectMapper.Instance.Map<Vector3>(data);
					RunChange_serverPosition(timestep);
				}
			}
		}

		public override void InterpolateUpdate()
		{
			if (IsOwner)
				return;

			if (serverPositionInterpolation.Enabled && !serverPositionInterpolation.current.UnityNear(serverPositionInterpolation.target, 0.0015f))
			{
				_serverPosition = (Vector3)serverPositionInterpolation.Interpolate();
				//RunChange_serverPosition(serverPositionInterpolation.Timestep);
			}
		}

		private void Initialize()
		{
			if (readDirtyFlags == null)
				readDirtyFlags = new byte[1];

		}

		public PixieType_1NetworkObject() : base() { Initialize(); }
		public PixieType_1NetworkObject(NetWorker networker, INetworkBehavior networkBehavior = null, int createCode = 0, byte[] metadata = null) : base(networker, networkBehavior, createCode, metadata) { Initialize(); }
		public PixieType_1NetworkObject(NetWorker networker, uint serverId, FrameStream frame) : base(networker, serverId, frame) { Initialize(); }

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}
