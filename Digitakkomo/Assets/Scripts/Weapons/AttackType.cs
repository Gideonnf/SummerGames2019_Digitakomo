﻿public enum AttackType
{
    UNKNOWN = -1,
    ICE = 0,
    FIRE = 1
}