﻿using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    [Range(0, 50)]
    public float MinDamage;
    public float MaxDamage;
    private float actualDamage;
    public float speed = 5.0f;
    public float timeoutDestructor = 5 * 60;
    private Rigidbody2D rb;
    public AttackType at = AttackType.UNKNOWN;
    public float nonAttackTypeDivider = 15;

    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        actualDamage = Random.Range(MinDamage, MaxDamage);
    }

    private void FixedUpdate()
    {
        if (timeoutDestructor <= 0)
        {
            Destroy(gameObject);
        }
        else
        {
            timeoutDestructor -= 1;
        }
    }

    public void SetTimeOutDestructor(float second)
    {
        timeoutDestructor = second * 60;
    }

    public void SetMissileDirection(int x, int y)
    {
        rb.velocity = new Vector2(x * speed, y * speed);
    }

    public void SetRotation(int rotation)
    {
        transform.Rotate(0, 0, rotation);
    }

    public float GetDamageWithAttackType(AttackType at)
    {
        if (this.at == at)
        {
            return actualDamage / nonAttackTypeDivider;
        }
        return actualDamage;
    }

    public float GetActualDamage()
    {
        return actualDamage;
    }
}
