﻿using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : EnemyManagerBehavior
{
    // Class for spawning Zone
    [System.Serializable]
    class SpawningZone
    {
        [Header("Zone Width and Height")]
        public Transform centerPoint = null;
        public Vector2 zoneDimensions = Vector2.zero;
        [Header("Waypoint Group Index to use")]
        public int waypointGroupIndex = 0;
    }
    // Class used to contain the spawning Template
    [System.Serializable]
    class SpawningTemplate
    {
        // The Enemy Prefab to spawn when timer reaches 0
        public GameObject enemyToSpawn = null;
        // How often to spawn the Enemy
        [Header("Spawn Intervals")]
        public float spawnInterval = 0.0f;
        float spawnTimer = 1.0f;
        int spawnZoneIndex = -1;    // Used to record down the last spawnZone Index
        [Header("Max Spawn Count")]
        public int maxSpawnCount = 1;
        int spawnCounter = 0;
        // Where to spawn the Enemy
        [Header("Spawn Zone")]
        public List<SpawningZone> listOfZones = new List<SpawningZone>();
        // Debug
        [Header("DEBUG")]
        public bool renderDebug = false;


        // Updates the Spawn Timer and when timer reaches 0
        // Returns true, if not always returns false
        public bool UpdateTimer()
        {
            // Have we spawned all enemies
            if (spawnCounter == maxSpawnCount)
                return false;
            // Count down Timer
            spawnTimer -= Time.deltaTime;
            if (spawnTimer > 0.0f)
                return false;

            // Increment Counter
            spawnCounter++;
            // Reset Timer
            spawnTimer = spawnInterval;
            return true;
        }
        // Returns a random position from the spawning Zones, if provided.
        // If not, returns Vector3.zero;
        public Vector3 GetRandomPosition()
        {
            // Cache the Selected Spawn Zone
            spawnZoneIndex = Random.Range(0, listOfZones.Count - 1);

            // If not center point to referrnce from
            if (listOfZones[spawnZoneIndex].centerPoint == null)
                return Vector3.zero;
            // Get a random position
            float xPos = Random.Range(listOfZones[spawnZoneIndex].centerPoint.position.x - listOfZones[spawnZoneIndex].zoneDimensions.x, listOfZones[spawnZoneIndex].centerPoint.position.x + listOfZones[spawnZoneIndex].zoneDimensions.x);
            float yPos = Random.Range(listOfZones[spawnZoneIndex].centerPoint.position.y - listOfZones[spawnZoneIndex].zoneDimensions.y, listOfZones[spawnZoneIndex].centerPoint.position.y + listOfZones[spawnZoneIndex].zoneDimensions.y);
            // Return the new position
            return new Vector3(xPos, yPos, 1.0f);
        }
        // Returns the Waypoint Group Index of the last SpawnZone you used
        public int GetLastSpawnZone_WaypointGroupIndex()
        {
            return listOfZones[spawnZoneIndex].waypointGroupIndex;
        }
        // Used to check if we have spawned all the enemies from this template
        public bool SpawnedAllEnemies()
        {
            if (spawnCounter >= maxSpawnCount)
                return true;
            return false;
        }
        // Returns if we want to draw the Debug Zone
        public bool GetRenderDebug()
        {
            return renderDebug;
        }
    }

    // List used to contain all the spawning Template
    [SerializeField]
    List<SpawningTemplate> listOfSpawns = new List<SpawningTemplate>();

    // Network related
    NetWorker networker;
    private void Start()
    {
        // Get referrence to the networker
        if (networkObject != null)
            if(networkObject.IsServer)
                networker = networkObject.Networker;
    }

    // Update is called once per frame
    void Update()
    {
        // IF unity's Update runs bfr the Object is instantiated in the network
        if (networkObject != null)
        {
            // If not Server, don't update
            if (!networkObject.IsServer)
                return;
            // Wait until both players connect
            if (networker.Players.Count < 2)
                return;
        }

        // Update Timer for all the Spawning Templates
        for (int i = 0; i < listOfSpawns.Count; ++i)
        {
            // have we spawned all enemies?
            if(!listOfSpawns[i].SpawnedAllEnemies())
            {
                // is it time to spawn and send the RPC call?
                if(listOfSpawns[i].UpdateTimer())
                {
                    // Are we in multiplayer mode?
                    if (networkObject != null)
                    {
                        //networkObject.SendRpc(RPC_SPAWN_ENEMY, Receivers.All, i, listOfSpawns[i].GetRandomPosition());
                        // Create object on network
                        PixieType_1Behavior enemyBehavior = NetworkManager.Instance.InstantiatePixieType_1(0, listOfSpawns[i].GetRandomPosition(), Quaternion.identity);
                        // set the waypoints groups
                        enemyBehavior.gameObject.GetComponent<PixieType1Script>().SetWaypointGroup(listOfSpawns[i].GetLastSpawnZone_WaypointGroupIndex());
                    }
                        
                    else
                    {
                        // Create new enemy
                        GameObject newEnemy = Instantiate(listOfSpawns[i].enemyToSpawn, listOfSpawns[i].GetRandomPosition(), Quaternion.identity);
                    }    
                }
            }
                

        }        
    }


    private void OnDrawGizmos()
    {
        // Change the color
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        // Check if we want to render the DebugCube 
        foreach (SpawningTemplate template in listOfSpawns)
        {
            if (!template.GetRenderDebug())
                continue;

            foreach (SpawningZone zone in template.listOfZones)
            {
                Gizmos.DrawWireCube(zone.centerPoint.position, zone.zoneDimensions * 2.0f);
            }
        }
    }



    // RPC Functions
    #region RPC CALLS
    // Called by the server when it detects that it is time to spawn a enemy
    public override void SpawnEnemy(RpcArgs args)
    {
        // Make sures that this is ran on the MainThread as we are interacting with Unity Objects
        // and not on the read or write thread that Forge creates
        MainThreadManager.Run(() =>
        {
            // Cache the ID 
            int templateID = args.GetNext<int>();
            // Check if EnemyObject is valid
            if (listOfSpawns[templateID].enemyToSpawn == null)
                return;

            // Create new enemy
            Instantiate(listOfSpawns[templateID].enemyToSpawn, args.GetNext<Vector3>(), Quaternion.identity);
        });
    }
    #endregion
}
