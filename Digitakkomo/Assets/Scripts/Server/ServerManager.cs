﻿using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;

public class ServerManager : MonoBehaviour
{
    public GameObject SinglePlayerObject;
    FollowPlayer CameraScript;
    private static bool IsMultiplayer = false;

    public static void SetIsMultiplayer(bool IsMultiplayer)
    {
        ServerManager.IsMultiplayer = IsMultiplayer;
    }

    public static bool GetIsMultiplayer()
    {
        return IsMultiplayer;
    }

    // Start is called before the first frame update
    void Start()
    {
        CameraScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<FollowPlayer>();
        if (IsMultiplayer)
        {
            Debug.Log("Instantiate MuliplayerPlayer");
            PlayerController player = (PlayerController)NetworkManager.Instance.InstantiateMovePlayer();
            CameraScript.SetPlayerCamera(player);
        }
        else
        {
            Debug.Log("Instantiate SinglePlayer");
            GameObject player = Instantiate(SinglePlayerObject);
            CameraScript.SetPlayerCamera(player);
        }


    }

    // Update is called once per frame
    void Update()
    {

    }
}
