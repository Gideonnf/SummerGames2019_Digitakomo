﻿using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    private Transform player;

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            return;
        }
        Debug.Log(player.position);

        Vector3 cameraPosition = new Vector3(player.position.x, player.position.y, transform.position.z);

        transform.position = cameraPosition;
    }

    public void SetPlayerCamera(GameObject player)
    {
        this.player = player.transform;
    }

    public void SetPlayerCamera(PlayerController player)
    {
        this.player = player.transform;
    }
}
