﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainScene : MonoBehaviour
{
    public Button SingleplayerButton;
    public Button MultiplayerButton;

    // Start is called before the first frame update
    void Start()
    {
        SingleplayerButton.onClick.AddListener(() =>
       {
           ServerManager.SetIsMultiplayer(false);
           SceneManager.LoadScene("Scenes/WeiTestingScene");
       });

        MultiplayerButton.onClick.AddListener(() =>
        {
            ServerManager.SetIsMultiplayer(true);
            SceneManager.LoadScene("MultiplayerHostJoin");
        });
    }

    // Update is called once per frame
    void Update()
    {

    }
}
