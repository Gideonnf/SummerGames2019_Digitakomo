﻿using UnityEngine;

public abstract class Attack : MonoBehaviour
{
    protected float MinDamage = 10.0f;
    protected float MaxDamage = 15.0f;
    protected float ActualDamge;
    private Animator Animate;
    public GameObject leftArm;
    public GameObject rightArm;
    public Collider2D AttackBlock;

    private bool Attacking = false;
    protected int direction = 0;

    void Awake()
    {
        Animate = GetComponent<Animator>();
        ActualDamge = Random.Range(MinDamage, MaxDamage);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            if (direction != 1)
            {
                transform.rotation *= Quaternion.Euler(0, 180, 0);
            }
            direction = 1;
        }
        if (Input.GetKey(KeyCode.D))
        {
            if (direction != 0)
            {
                transform.rotation *= Quaternion.Euler(0, 180, 0);
            }
            direction = 0;
        }
        if (Input.GetMouseButtonDown(0))
        {
            DoAttack();
            Attacking = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            Attacking = false;
        }
        AttackBlock.enabled = Attacking;
        Animate.SetBool("Attacking", Attacking);
    }

    protected virtual void FixedUpdate() { }

    protected abstract void DoAttack();
}
