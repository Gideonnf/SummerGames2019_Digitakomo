﻿using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;

public abstract class SpecialAttack_Abstract : AttackCreationBehavior
{
    protected bool WPressed = false;
    protected bool APressed = false;
    protected bool DPressed = false;
    protected int latestDirection = 0;

    // Update is called once per frame
    protected void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            WPressed = true;
        }
        else if (Input.GetKeyUp(KeyCode.W))
        {
            WPressed = false;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            APressed = true;
        }
        else if (Input.GetKeyUp(KeyCode.A))
        {
            APressed = false;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            DPressed = true;
        }
        else if (Input.GetKeyUp(KeyCode.D))
        {
            DPressed = false;
        }

        if (ServerManager.GetIsMultiplayer())
        {
            if (networkObject == null)
            {
                return;
            }
            if (!networkObject.IsOwner)
            {
                return;
            }
            if (Input.GetMouseButtonDown(1))
            {
                networkObject.SendRpc(RPC_ATTACK, Receivers.All, "IceMissile", GetCreatePosition(), GetAttackDirection(), GetIsCreated());
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(1))
            {
                Attack("IceMissile", GetCreatePosition(), GetAttackDirection(), GetIsCreated());
            }
        }
    }

    protected abstract void Attack(string name, Vector3 createPosition, int direction, bool isCreated);

    private Vector3 GetCreatePosition()
    {
        return transform.position;
    }

    /**
     *     2
     *   1   3
     * 0       4
     **/
    private int GetAttackDirection()
    {
        if (!WPressed && !APressed && !DPressed)
        {
            return latestDirection;
        }
        if (WPressed)
        {
            if (APressed)
            {
                latestDirection = 1;
                return latestDirection;
            }
            else if (DPressed)
            {
                latestDirection = 3;
                return latestDirection;
            }
            else
            {
                latestDirection = 2;
                return latestDirection;
            }
        }
        else
        {
            if (APressed)
            {
                latestDirection = 0;
                return latestDirection;
            }
            else
            {
                latestDirection = 4;
                return latestDirection;
            }
        }
    }

    private bool GetIsCreated()
    {
        if (APressed && DPressed)
        {
            return false;
        }
        return true;
    }
}
