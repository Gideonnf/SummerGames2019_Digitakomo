﻿using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;

public class IceMissileShooting : SpecialAttack_Abstract
{
    // Update is called once per frame

    public IceMissile iceMissile;
    private void Shoot(Vector3 createPosition, int direction)
    {
        IceMissile clone = Instantiate(iceMissile, createPosition, this.transform.rotation);

        int xDir = 0;
        int yDir = 0;
        int rotation = 0;
        switch (direction)
        {
            case 0: xDir = -1; break;
            case 1: xDir = -1; yDir = 1; rotation = 45; break;
            case 2: yDir = 1; rotation = 90; break;
            case 3: xDir = 1; yDir = 1; rotation = 45; break;
            case 4: xDir = 1; break;
        }
        clone.SetRotation(rotation);
        clone.SetTimeOutDestructor(1);
        clone.SetMissileDirection(xDir, yDir);
    }

    public override void Attack(RpcArgs args)
    {
        MainThreadManager.Run(() =>
        {
            string name = args.GetNext<string>();
            Vector3 createPosition = args.GetNext<Vector3>();
            int direction = args.GetNext<int>();
            bool isCreated = args.GetNext<bool>();

            Attack(name, createPosition, direction, isCreated);
        });
    }

    protected override void Attack(string name, Vector3 createPosition, int direction, bool isCreated)
    {
        if (!isCreated || name != "IceMissile")
        {
            return;
        }

        Shoot(createPosition, direction);
    }
}
