﻿using UnityEngine;

public class PlayerController : PlayerInput_Abstract
{

    void Start()
    {
        myRb2D = GetComponent<Rigidbody2D>();
        // Calculate how many jumps we can do
        jumpsLeft = 1 + extraJumps;
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        // if we are in multiplayer mode
        if (ServerManager.GetIsMultiplayer())
        {
            if (networkObject == null)
                return;
            //// IF we are not the owner of this object
            if (!networkObject.IsOwner)
            {
                // Apply new data from network and return
                myRb2D.position = networkObject.serverPosition;
                return;
            }
        }
        //// IF unity's Update runs bfr the Object is instantiated in the network

        // Get latest Horizontal Input from player
        horizontalInput = Input.GetAxisRaw("Horizontal");

        // Increase gravity if player has jumped and is currently falling
        if (myRb2D.velocity.y < 0)
            myRb2D.velocity += Vector2.up * (Physics2D.gravity.y * fallingMultiplyer) * Time.deltaTime;
        // If player wants to jump
        if (Input.GetButtonDown("Jump"))
            Jump();

    }
    // Fixed Update called every physics Update
    void FixedUpdate()
    {
        if (ServerManager.GetIsMultiplayer())
        {
            //// IF unity's Update runs bfr the Object is instantiated in the network
            if (networkObject == null)
                return;
            //// IF we are not the owner of this object
            if (!networkObject.IsOwner)
            {
                return;
            }
        }

        // IF WE ARE THE OWNER OF THIS OBJECT..
        // Check if we are grounded
        CheckGrounded();

        if (horizontalInput == 0.0f)
            StopHorizontalMovement();
        else
        {
            if (!isLockMovement)
            {
                MoveHorizontal();
            }
        }


        if (ServerManager.GetIsMultiplayer())
        {
            networkObject.serverPosition = myRb2D.position;
        }
    }


    // Flip Player when changing direction
    void Flip()
    {
        // Clear the existing Velocity
        myRb2D.velocity = myRb2D.velocity * 0.4f;
    }


    // Player moving
    public override void MoveHorizontal()
    {
        // Check if we need to change direction
        if (horizontalInput > 0 && !facingRight)
        {
            facingRight = true;
            Flip();
        }
        else if (horizontalInput < 0 && facingRight)
        {
            facingRight = false;
            Flip();
        }

        // Check if we can continue to accelerate
        if (horizontalInput * myRb2D.velocity.x < maxMoveSpeed)
            myRb2D.AddForce(Vector2.right * horizontalInput * moveAcceleration * Time.deltaTime);

        // Clamp the speed within the maxMoveSpeed
        if (Mathf.Abs(myRb2D.velocity.x) > maxMoveSpeed)
            myRb2D.velocity = new Vector2(Mathf.Sign(myRb2D.velocity.x) * maxMoveSpeed, myRb2D.velocity.y);

        // Apply newest translation to the server
    }
    // Player Jumping
    public override void Jump()
    {
        // No more jumps left and still not grounded
        if (jumpsLeft <= 0 && !isGrounded)
            return;
        else if (jumpsLeft >= 0 && isGrounded)  // Grounded, so reset jumps
            jumpsLeft = extraJumps + 1;

        // Jump
        myRb2D.velocity = Vector2.up * jumpAcceleration;

        // Reduce number of jumps
        jumpsLeft--;
    }
}
