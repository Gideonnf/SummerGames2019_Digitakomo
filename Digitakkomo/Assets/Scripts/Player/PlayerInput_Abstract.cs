﻿using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;

public abstract class PlayerInput_Abstract : MovePlayerBehavior
{
    // Data that all characters share
    [Header("Common Data shared by all characters")]
    [SerializeField]
    protected float moveAcceleration = 700.0f;    // How fast does the character move
    [SerializeField]
    protected float maxMoveSpeed = 5.0f;      // The max speed the player can move
    public float horizontalInput = 0.0f;   // Used to cache the horizontal input
    protected bool facingRight = false;  // Bool to check if we are facing right or left
    // Jumping related
    [SerializeField]
    protected float jumpAcceleration = 10.0f;      // How much force does the character use to jump
    public bool isGrounded = true;      // Whether this character is grounded
    [SerializeField]
    Transform groundCheck = null;       // Where to start detecting is ground
    [SerializeField]
    float groundCheckRadius = 0.5f;     // The radius to detect the ground
    [SerializeField]
    LayerMask whatIsGround;     // What layer to detect as ground
    [SerializeField]
    protected int extraJumps = 1;       // How many extra jumps we get, not including default
    protected int jumpsLeft = 1;
    [SerializeField]
    protected float fallingMultiplyer = 2.0f;   // How much fast should this character fall

    protected bool isLockMovement = false; // shift to lock movement

    // Unity Stuff
    protected Rigidbody2D myRb2D = null;

    // Start is called before the first frame update
    void Start()
    {
        myRb2D = GetComponent<Rigidbody2D>();
    }

    protected virtual void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            isLockMovement = true;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            isLockMovement = false;
        }
    }

    // Moving Horizontal, left and right
    public virtual void MoveHorizontal() { }
    // Jumping
    public virtual void Jump() { }


    // Check if we are grounded, are we touching the ground?
    protected void CheckGrounded()
    {
        // Check if we are grounded
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

        Debug.DrawLine(groundCheck.position, new Vector3(groundCheck.position.x + groundCheckRadius, groundCheck.position.y + groundCheckRadius, 1.0f), Color.red);
    }
    // Stop all horizontal Velocities of the rigidBody
    protected void StopHorizontalMovement()
    {
        myRb2D.velocity = new Vector2(0.0f, myRb2D.velocity.y);
    }
    // Stop all Vertical Velocities of the rigidBody
    protected void StopVerticalMovement()
    {
        myRb2D.velocity = new Vector2(myRb2D.velocity.x, 0.0f);
    }
}
