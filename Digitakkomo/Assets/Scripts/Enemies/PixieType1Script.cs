﻿using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;

public class PixieType1Script : PixieType_1Behavior
{
    // Movement Data
    [Header("Movement Data")]
    [SerializeField]
    float moveSpeed = 1.0f;
    [SerializeField]
    float stoppingInterval = 5.0f;

    // Unity Stuff
    Rigidbody2D myRb2D = null;

    // Used to identify which wayPoint group are we assigned to
    public int waypointGroup = -1;
    int currentWaypoint = 0;
    Vector2 targetPosition = Vector2.zero;
    Vector2 direction = Vector2.zero;


    // Start is called before the first frame update
    void Start()
    {
        myRb2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // Only server will Update
        if (!networkObject.IsServer)
        {
            // Just apply data from the server
            myRb2D.position = networkObject.serverPosition;
            return;
        }

        // if no waypoint yet, don't update
        if (waypointGroup == -1)
            return;

        // Move towards waypoint
        direction = targetPosition - myRb2D.position;
        myRb2D.MovePosition(myRb2D.position + (direction.normalized * moveSpeed * Time.deltaTime));
        // Check if we are reaching waypoint
        CheckNextWaypoint();

        // Send newdata to server
        if (networkObject != null)
            UpdateMultiplayer();
    }

    // Updates multiplayer data 
    void UpdateMultiplayer()
    {
        // Position
        networkObject.serverPosition = myRb2D.position;
    }

    // Checks if we need to go to next waypoint
    void CheckNextWaypoint()
    {
        // Check if we have reached the waypoint
        if ((targetPosition - myRb2D.position).sqrMagnitude > 2.0f)
            return;

        // Get the next waypoint
        WaypointGroupManager.WaypointReturnData temp = WaypointGroupManager.instance.GetNextWaypoint_Wrapped(waypointGroup, currentWaypoint);
        targetPosition = temp.nextPosition;
        currentWaypoint = temp.nextWaypointIndex;
    }


    // Sets the object's Waypoint Group
    public void SetWaypointGroup(int newWaypointGroupID)
    {
        // if already set, then return;
        if (waypointGroup != -1)
            return;
        waypointGroup = newWaypointGroupID;

        // Get the first waypoint position
        targetPosition = WaypointGroupManager.instance.GetWaypoint(waypointGroup, 0).position;
        currentWaypoint = 0;
    }
}
